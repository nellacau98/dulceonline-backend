const mongoose = require ('mongoose');

const productoSchema = mongoose.Schema({
    nombre_producto:{type: String, maxLengt:80, required: true, unique: true},
    categoria:[{type: String, maxLengt:80, required: true, unique: false}],
    proveedor:{type: String,maxLengt:80, required: true, unique: false},
    cantidad_disponible:{type: Number, required: true, unique: false},
    precio_compra:{type: Number, maxLengt:190, required: true, unique: false},
    precio_venta:{type: Number, maxLengt:190, required: true, unique: false},
    fecha_registro_producto:{type: String, maxLengt:80, required: true, unique: false},
    imagen: { type:String, required:true, unique: true },
    disp: { type:Boolean, default:true }
})

module.exports = mongoose.model("productos",productoSchema)