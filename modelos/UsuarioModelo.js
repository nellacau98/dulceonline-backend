const mongoose = require ('mongoose');

const usuarioSchema = mongoose.Schema({
    tipo_usuario:{type:String,maxLength:20, required: true, unique:false},
    nombres_usuario:{type: String, maxLengt:80, required: true, unique: false},
    apellidos_usuario:{type: String, maxLengt:50, required: true, unique: false},
    tipo_documento_usuario:{type: String, maxLengt:3, required: true, unique: false},
    documento_usuario:{type: String, required: true, unique: true},
    direccion_usuario:{type: String, maxLengt:190, required: true, unique: false},
    ciudad_usuario:{type: String, maxLengt:90, required:true, unique: false},
    telefono_usuario:{type: Number, required: true, unique: false},
    correo_usuario:{type: String, maxLengt:190, required: true, unique: true},
    passw:{type: String, maxLengt:190, required: true, unique: false}
})

module.exports = mongoose.model("usuarios",usuarioSchema)