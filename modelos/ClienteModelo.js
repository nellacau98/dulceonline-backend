const mongoose = require ('mongoose');

const clienteSchema = mongoose.Schema({
    nombres:{type: String, maxLengt:80, required: true, unique: false},
    apellidos:{type: String, maxLengt:50, required: true, unique: false},
    tipo_documento:{type: String, maxLengt:3, required: true, unique: false},
    documento:{type: String, required: true, unique: true},
    direccion:{type: String, maxLengt:190, required: true, unique: false},
    ciudad:{type: String, maxLengt:90, required:true, unique: false},
    telefono:{type: String, required: true, unique: false},
    email:{type: String, maxLengt:190, required: true, unique: true},
    passw:{type: String, maxLengt:190, required: true, unique: false},
    es_admin: { type:Boolean, required:true }
})

module.exports = mongoose.model("clientes",clienteSchema)