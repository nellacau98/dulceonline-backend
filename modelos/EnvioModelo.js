const mongoose = require ('mongoose');

const envioSchema = mongoose.Schema({
    ciudad:{type: String, maxLengt:80, required: true, unique: false},
    departamento:{type: String, maxLengt:50, required: true, unique: false},
    precio_envio:{type: Number, maxLengt:90, required: true, unique: false}
})

module.exports = mongoose.model("envios",envioSchema)