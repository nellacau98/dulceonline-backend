const mongoose = require ('mongoose');

const pedidoSchema = mongoose.Schema({
    pedido:{type: Array, maxLengt:190, required: true, unique: false},
    precio_venta:{type: Number, maxLengt:50, required: true, unique: false},
    precio_envio:{type: Number, maxLengt:3, required: true, unique: false},
    total_pedido:{type: Number, maxLengt:3, required: true, unique: false},
    fecha_pedido:{type: String,maxLengt:80, required: true, unique: false},
   
})

module.exports = mongoose.model("pedidos",pedidoSchema)