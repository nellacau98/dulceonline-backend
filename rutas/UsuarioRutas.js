const usuarioOperaciones = require("../operaciones/UsuarioOperaciones");
const router = require("express").Router();

router.get("/", usuarioOperaciones.buscarUsuarios);
router.post("/", usuarioOperaciones.crearUsuario);
router.get("/:id", usuarioOperaciones.buscarUsuario);
router.put("/:id", usuarioOperaciones.modificarUsuario);
router.delete("/:id", usuarioOperaciones.borrarUsuario);

module.exports = router;