const ClienteOperaciones = require("../operaciones/ClienteOperaciones");
const router = require('express').Router();

router.post("/", ClienteOperaciones.crearCliente);
router.get("/", ClienteOperaciones.buscarClientes);
router.get("/:id", ClienteOperaciones.buscarCliente);
router.put("/:id", ClienteOperaciones.modificarCliente);
router.delete("/:id", ClienteOperaciones.borrarCliente);
module.exports = router;