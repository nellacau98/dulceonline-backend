const envioOperaciones = require("../operaciones/EnvioOperaciones");
const router = require("express").Router();

router.get("/", envioOperaciones.buscarEnvios);
router.post("/", envioOperaciones.crearEnvio);
router.get("/:id", envioOperaciones.buscarEnvio);
router.put("/:id", envioOperaciones.modificarEnvio);
router.delete("/:id", envioOperaciones.borrarEnvio);

module.exports = router;