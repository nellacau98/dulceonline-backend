const pedidoModelo = require("../modelos/PedidoModelo");
const pedidoOperaciones = {}

pedidoOperaciones.crearPedido = async (req, res) => {
    try {
        const objeto = req.body;
        console.log(objeto);
        const pedido = new pedidoModelo(objeto);
        const pedidoGuardado = await pedido.save();
        res.status(201).send(pedidoGuardado);
    } catch (error) {
        res.status(400).send("Mala petición. " + error);
    }
}

pedidoOperaciones.buscarPedidos = async (req, res) => {
    try {
        const filtro = req.query;
        let listapedidos;
        if (filtro.q != null) {
            listapedidos = await pedidoModelo.find({
                "$or": [
                    { "pedido": { $regex: filtro.q, $options: "i" } },
                    { "fecha_pedido": { $regex: filtro.q, $options: "i" } },
                ]
            });
        }
        else {
            listapedidos = await pedidoModelo.find(filtro);
        }
        if (listapedidos.length > 0) {
            res.status(200).send(listapedidos);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. " + error);
    }

}

pedidoOperaciones.buscarPedido = async (req, res) => {
    try {
        const id = req.params.id;
        const pedido = await pedidoModelo.findById(id);
        if (pedido != null) {
            res.status(200).send(pedido);
        } else {
            res.status(404).send("No hay datos.");
        }
    } catch (error) {
        res.status(400).send("Mala petición." + error);
    }

}

pedidoOperaciones.modificarPedido = async (req, res) => {
    try {
        const id = req.params.id;
        const body = req.body;
        const datosActualizar = {
            pedido: body.pedido,
            precio_venta: body.precio_venta,
            precio_envio: body.precio_envio,
            fecha_pedido: body.fecha_pedido,
            total_pedido: body.total_pedido,
        }
        const pedidoActualizado = await pedidoModelo.findByIdAndUpdate(id, datosActualizar, { new: true })
        if (pedidoActualizado != null) {
            res.status(200).send(pedidoActualizado);
        }
        else {
            res.status(404).send("No hay datos.");
        }
    } catch (error) {
        res.status(400).send("Mala petición." + error);
    }
}

pedidoOperaciones.borrarPedido = async (req, res) => {
    try {
        const id = req.params.id;
        const pedido = await pedidoModelo.findByIdAndDelete(id);
        if (pedido != null) {
            res.status(200).send(pedido);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. " + error);
    }
}



module.exports = pedidoOperaciones;    
