const envioModelo = require("../modelos/EnvioModelo");
const envioOperaciones = {}

envioOperaciones.crearEnvio = async (req, res)=>{
    try {
        const objeto = req.body;
        console.log(objeto);
        const envio = new envioModelo(objeto);
        const envioGuardado = await envio.save();
        res.status(201).send(envioGuardado);
    } catch (error) {
        res.status(400).send("Mala petición. "+error);
    }
}

envioOperaciones.buscarEnvios = async (req, res)=>{
    try {
        const filtro = req.query;
        let listaenvios;
        if (filtro.q != null) {
            listaenvios = await envioModelo.find({
                "$or" : [ 
                    { "ciudad": { $regex:filtro.q, $options:"i" }},
                    { "departamento": { $regex:filtro.q, $options:"i" }}//,
                    //{ "direccion": { $regex:filtro.q, $options:"i" }}
                ]
            });
        }
        else {
            listaenvios = await envioModelo.find(filtro);
        }
        if (listaenvios.length > 0){
            res.status(200).send(listaenvios);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. "+error);
    }
}

envioOperaciones.buscarEnvio = async (req, res)=>{
    try {
        const id = req.params.id;
        const envio = await envioModelo.findById(id);
        if (envio != null){
            res.status(200).send(envio);
        } else {
            res.status(404).send("No hay datos.");
        }
    } catch (error) {
        res.status(400).send("Mala petición." + error);
    }
}

envioOperaciones.modificarEnvio = async (req, res)=>{
    try{
        const id =req.params.id;
        const body = req.body;
        const datosActualizar ={
            ciudad:body.ciudad,
            departamento:body.departamento,
            precio_envio:body.precio_envio

        }
        const envioActualizado = await envioModelo.findByIdAndUpdate(id,datosActualizar,{new : true})
        if (envioActualizado != null){
            res.status(200).send(envioActualizado);
        }
        else{
        res.status(404).send("No hay datos.");
        }
    } catch (error) {
        res.status(400).send("Mala petición."+ error);
}    
}

envioOperaciones.borrarEnvio = async (req, res)=>{
    try {
        const id = req.params.id;
        const envio = await envioModelo.findByIdAndDelete(id);
        if (envio != null){
            res.status(200).send(envio);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. "+error);
    }
}

module.exports = envioOperaciones;