const usuarioModelo = require("../modelos/UsuarioModelo");
const usuarioOperaciones = {}
const bcrypt = require("bcrypt");

const cifrarPassword = async (passw) => {
    const SALT_TIMES = 10;
    const salt = await bcrypt.genSalt(SALT_TIMES);
    return await bcrypt.hash(passw, salt);
}

usuarioOperaciones.crearUsuario = async (req, res) => {
    try {
        const objeto = req.body;
        objeto.passw = await cifrarPassword(objeto, passw);
        const usuario = new usuarioModelo(objeto);
        const usuarioGuardado = await usuario.save();
        res.status(201).send(usuarioGuardado);
    } catch (error) {
        res.status(400).send("Mala petición. " + error);
    }
}

usuarioOperaciones.buscarUsuario = async (req, res) => {
    try {
        const id = req.params.id;
        const usuario = await usuarioModelo.findById(id);
        if (usuario != null) {
            res.status(200).send(usuario);
        } else {
            res.status(404).send("No hay datos.");
        }
    } catch (error) {
        res.status(400).send("Mala petición." + error);
    }
}

usuarioOperaciones.modificarUsuario = async (req, res) => {
    try {
        const id = req.params.id;
        const body = req.body;
        if (body.passw != null) {
            body.passw = await cifrarPassword(body.passw);
        }
        const datosActualizar = {
            nombres_usuario: body.nombres_usuario,
            apellidos_usuario: body.apellidos_usuario,
            direccion_usuario: body.direccion_usuario,
            ciudad_usuario: body.ciudad_usuario,
            telefono_usuario: body.telefono_usuario,
            passw: body.passw
        }
        const usuarioActualizado = await usuarioModelo.findByIdAndUpdate(id, datosActualizar, { new: true })
        if (usuarioActualizado != null) {
            res.status(200).send(usuarioActualizado);
        }
        else {
            res.status(404).send("No hay datos.");
        }
    } catch (error) {
        res.status(400).send("Mala petición." + error);
    }
}

usuarioOperaciones.buscarUsuarios = async (req, res) => {
    try {
        const filtro = req.query;
        let listausuarios;
        if (filtro.q != null) {
            listausuarios = await usuarioModelo.find({
                "$or": [
                    { "nombres": { $regex: filtro.q, $options: "i" } },
                    { "apellidos": { $regex: filtro.q, $options: "i" } },
                    { "documento": { $regex: filtro.q, $options: "i" } },
                    { "direccion": { $regex: filtro.q, $options: "i" } },
                    { "correo": { $regex: filtro.q, $options: "i" } }
                ]
            });
        }
        else {
            listausuarios = await usuarioModelo.find(filtro);
        }
        if (listausuarios.length > 0) {
            res.status(200).send(listausuarios);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. " + error);
    }
}

usuarioOperaciones.borrarUsuario = async (req, res) => {
    try {
        const id = req.params.id;
        const pedido = await usuarioModelo.findByIdAndDelete(id);
        if (pedido != null) {
            res.status(200).send(pedido);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. " + error);
    }
}


module.exports = usuarioOperaciones;