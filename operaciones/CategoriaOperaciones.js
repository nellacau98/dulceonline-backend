const categoriaModelo = require("../modelos/CategoriaModelo");
const categoriaOperaciones = {}

categoriaOperaciones.crearCategoria = async (req, res)=>{
    try {
        const objeto = req.body;
        console.log(objeto);
        const categoria = new categoriaModelo(objeto);
        const categoriaGuardado = await categoria.save();
        res.status(201).send(categoriaGuardado);
    } catch (error) {
        res.status(400).send("Mala petición. "+error);
    }
}

categoriaOperaciones.buscarCategorias = async (req, res)=>{
    try {
        const filtro = req.query;
        let listacategorias;
        if (filtro.q != null) {
            listacategorias = await categoriaModelo.find({
                "$or" : [ 
                    { "nombre_categoria": { $regex:filtro.q, $options:"i" }}
                ]
            });
        }
        else {
            listacategorias = await categoriaModelo.find(filtro);
        }
        if (listacategorias.length > 0){
            res.status(200).send(listacategorias);
        } else {
            res.status(404).send("No hay datos"+error);
        }
    } catch (error) {
        res.status(400).send("Mala petición. "+error);
    }
}

categoriaOperaciones.buscarCategoria = async (req, res)=>{
    try {
        const id = req.params.id;
        const categoria = await categoriaModelo.findById(id);
        if (categoria != null){
            res.status(200).send(categoria);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. "+error);
    }
}

categoriaOperaciones.modificarCategoria = async (req, res)=>{
    try {
        const id = req.params.id;
        const body = req.body;
        const datosActualizar = {
            nombre_categoria: body.nombre_categoria,
        }
        const categoriaActualizado = await categoriaModelo.findByIdAndUpdate(id, datosActualizar, { new : true });
        if (categoriaActualizado != null) {
            res.status(200).send(categoriaActualizado);
        }
        else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. "+error);
    }
}

categoriaOperaciones.borrarCategoria = async (req, res)=>{
    try {
        const id = req.params.id;
        const categoria = await categoriaModelo.findByIdAndDelete(id);
        if (categoria != null){
            res.status(200).send(categoria);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. "+error);
    }
}

module.exports = categoriaOperaciones;
