const mongoose = require ("mongoose");

const username = "admin";
const password = "admin";
const database = "MidulceonlineDB";
const URI = "mongodb://"+username+":"+password+"@ac-13nzrc3-shard-00-00.uqda3xz.mongodb.net:27017,ac-13nzrc3-shard-00-01.uqda3xz.mongodb.net:27017,ac-13nzrc3-shard-00-02.uqda3xz.mongodb.net:27017/"+database+"?ssl=true&replicaSet=atlas-g4vvh2-shard-0&authSource=admin&retryWrites=true&w=majority";
const conectar = async () => {
    try {
        await mongoose.connect(URI);
        console.log("Atlas está en linea");
    } catch (error) {
        console.log("Error de conexión. "+error);
    }
    /*
    mongoose.connect(URI)
        .then(()=>{ console.log("Atlas está en linea"); })
        .catch(()=>{ console.log("Error de conexión. "+error); })
    */
}
conectar();

module.exports = mongoose;
